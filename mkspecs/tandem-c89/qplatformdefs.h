/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the qmake spec of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QPLATFORMDEFS_H
#define QPLATFORMDEFS_H

#ifdef UNICODE
#ifndef _UNICODE
#define _UNICODE
#endif
#endif

// Get Qt defines/settings

#include <QtCore/qglobal.h>

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <limits.h>

#ifndef __GUARDIAN_TARGET
#include <spthread.h>
#endif

#ifdef _GUARDIAN_TARGET
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netdb.h>
#include <stropts.h>
//typedef int socklen_t;
#define QT_NO_GETIFADDRS
#ifdef _GUARDIAN_TARGET
#define Q_NO_POSIX_SIGNALS
#endif
#endif

#define Q_FS_FAT
#ifdef QT_LARGEFILE_SUPPORT
#define QT_STATBUF		struct _stati64		// non-ANSI defs
#define QT_STATBUF4TSTAT	struct _stati64		// non-ANSI defs
#define QT_STAT			::_stati64
#define QT_FSTAT		::_fstati64
#else
#define QT_STATBUF		struct _stat		// non-ANSI defs
#define QT_STATBUF4TSTAT	struct _stat		// non-ANSI defs
#define QT_STAT			::stat
#define QT_FSTAT		::fstat
#endif
#define QT_STAT_REG		S_IFREG
#define QT_STAT_DIR		S_IFDIR
#define QT_STAT_MASK		S_IFMT
#if defined(_S_IFLNK)
#  define QT_STAT_LNK		S_IFLNK
#endif
#define QT_FILENO		fileno
#define QT_OPEN			::open
#define QT_CLOSE		::close
#ifdef QT_LARGEFILE_SUPPORT
#define QT_LSEEK		::lseeki64
#ifndef UNICODE
#define QT_TSTAT		::stati64
#else
#define QT_TSTAT		::wstati64
#endif
#else
#define QT_LSEEK		::lseek
#ifndef UNICODE
#define QT_TSTAT		::stat
#else
#define QT_TSTAT		::wstat
#endif
#endif
#define QT_READ			::read
#define QT_WRITE		::write
#define QT_ACCESS		::access
#define QT_GETCWD		::getcwd
#define QT_CHDIR		::chdir
#define QT_MKDIR		::mkdir
#define QT_RMDIR		::rmdir
#define QT_OPEN_LARGEFILE       0
#define O_LARGEFILE         0
#define QT_OPEN_RDONLY		O_RDONLY
#define QT_OPEN_WRONLY		O_WRONLY
#define QT_OPEN_RDWR		O_RDWR
#define QT_OPEN_CREAT		O_CREAT
#define QT_OPEN_TRUNC		O_TRUNC
#define QT_OPEN_APPEND		O_APPEND
#if defined(O_TEXT)
# define QT_OPEN_TEXT		O_TEXT
# define QT_OPEN_BINARY		O_BINARY
#endif

#include "../common/c89/qplatformdefs.h"
#include "../common/posix/qplatformdefs.h"


#ifdef QT_LARGEFILE_SUPPORT
#undef QT_FSEEK
#undef QT_FTELL
#undef QT_OFF_T

#define QT_FSEEK                ::fseeko64
#define QT_FTELL                ::ftello64
#define QT_OFF_T                off64_t
#endif

#define QT_LSTAT                lstat
#define QT_SIGNAL_ARGS		int

#define QT_VSNPRINTF		::vsnprintf
#define QT_SNPRINTF		::_snprintf

# define F_OK	0
# define X_OK	1
# define W_OK	2
# define R_OK	4



#endif // QPLATFORMDEFS_H
