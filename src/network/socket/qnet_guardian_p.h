/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtNetwork module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QNET_GUARDIAN_P_H
#define QNET_GUARDIAN_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt code on Unix. This header file may change from version to
// version to version without notice, or even be removed.
//
// We mean it.
//

#include "private/qcore_unix_p.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#if defined(Q_OS_VXWORKS)
#  include <sockLib.h>
#endif

// for inet_addr
#include <netdb.h>
#include <arpa/inet.h>
#if defined(Q_OS_VXWORKS)
#  include <hostLib.h>
#else
#  include <resolv.h>
#endif

#ifdef _GUARDIAN_TARGET
#include <cextdecs.h>
#include <tal.h>
#include <sys/uio.h>
#include <sys/socket.h>

//typedef int socklen_t;                /* length of socket address struct */

#include <QtCore/QCoreApplication>

enum tag { socket_tag      = 1, 
           bind_tag        = 2, 
           acceptnw_tag    = 3, 
           acceptnw2_tag   = 4, 
           connect_tag     = 5,
           getpeername_tag = 6, 
           getsockname_tag = 7, 
           getsockopt_tag  = 8, 
           setsockopt_tag  = 9, 
           ioctl_tag       = 10, 
           recv_tag        = 11, 
           send_tag        = 12,
           receive_read_tag  = 13,
           receive_reply_tag = 14};

#endif

QT_BEGIN_NAMESPACE

// Almost always the same. If not, specify in qplatformdefs.h.
#if !defined(QT_SOCKOPTLEN_T)
# define QT_SOCKOPTLEN_T QT_SOCKLEN_T
#endif

// UnixWare 7 redefines socket -> _socket
static inline int qt_safe_socket(int domain, int type, int protocol, int flags = 0)
{
    Q_ASSERT((flags & ~O_NONBLOCK) == 0);

#ifdef _GUARDIAN_TARGET
    short fd;
#else
    register int fd;
#endif

#if defined(SOCK_CLOEXEC) && defined(SOCK_NONBLOCK)
    int newtype = type | SOCK_CLOEXEC;
    if (flags & O_NONBLOCK)
        newtype |= SOCK_NONBLOCK;
    fd = ::socket(domain, newtype, protocol);
    if (fd != -1 || errno != EINVAL)
        return fd;
#endif

#ifdef _GUARDIAN_TARGET
    fd =::socket_nw(domain, type, protocol, 0217 /*two nowaits*/, 0);
    if (fd < 0)
        return fd;

    int tag;
    AWAITIOX(&fd,,,&tag, -1);
    return fd;
#else
    fd = ::socket(domain, type, protocol);
#endif
    if (fd == -1)
        return -1;

    ::fcntl(fd, F_SETFD, FD_CLOEXEC);

    // set non-block too?
    if (flags & O_NONBLOCK)
        ::fcntl(fd, F_SETFL, ::fcntl(fd, F_GETFL) | O_NONBLOCK);

    return fd;
}

// Tru64 redefines accept -> _accept with _XOPEN_SOURCE_EXTENDED
#ifdef _GUARDIAN_TARGET
static inline int qt_safe_accept(short s, struct sockaddr *addr, QT_SOCKLEN_T *addrlen, int flags = 0)
#else
    static inline int qt_safe_accept(int s, struct sockaddr *addr, QT_SOCKLEN_T addrlen, int flags = 0)
#endif
{
    Q_ASSERT((flags & ~O_NONBLOCK) == 0);
#ifdef _GUARDIAN_TARGET
    register short fd;
#else
    register int fd;
#endif

#if QT_UNIX_SUPPORTS_THREADSAFE_CLOEXEC && defined(SOCK_CLOEXEC) && defined(SOCK_NONBLOCK)
    // use accept4
    int sockflags = SOCK_CLOEXEC;
    if (flags & O_NONBLOCK)
        sockflags |= SOCK_NONBLOCK;
    fd = ::accept4(s, addr, static_cast<QT_SOCKLEN_T *>(addrlen), sockflags);
    if (fd != -1 || !(errno == ENOSYS || errno == EINVAL))
        return fd;
#endif

#ifdef _GUARDIAN_TARGET

    if (QByteArray((char*)addr,16).toHex() == "00000000000000000000000000000000") {
        return -1;
    }

    fd =::socket_nw(AF_INET /*or UDP?*/, SOCK_STREAM, 0, 0217 , 0);
    if (fd < 0) {
        perror("error:");
        qFatal("could not create a new socket");
        return fd;
    }
    
    __int32_t      tag = -1;
    AWAITIOX(&fd,,,&tag, -1);
    

    SETMODE(fd,30);

    _cc_status     CC;
    bool done = false;

    CC = AWAITIOX(&fd,,,&tag, -1);

    short error = ::accept_nw2(fd, addr, acceptnw2_tag);
    if (error < 0) {
    	perror("::accept_nw2");
    	qFatal("::accept_nw2");
    }

    done = false;
    while (!done) {
        CC = AWAITIOX(&fd,,,&tag, 0);
        if (tag == acceptnw2_tag)
            done = true;
    }
    int   optval = 3*32767;
    short optlen = sizeof(optval);

    short err = setsockopt_nw(fd, SOL_SOCKET, SO_SNDBUF, (char*)&optval, optlen, setsockopt_tag);
    if (err < 0)
        perror("setsockopt_nw error");

    AWAITIOX(&fd,,,&tag,-1);

    err = setsockopt_nw(fd, SOL_SOCKET, SO_RCVBUF, (char*)&optval, optlen, setsockopt_tag);
    if (err < 0)
        perror("setsockopt_nw error");

    AWAITIOX(&fd,,,&tag,-1);

    error = ::accept_nw(s, addr, addrlen, acceptnw_tag);
    if (error < 0) {
     	perror("::accept_nw");
    	qFatal("::accept_nw error");
    }

    memset(addr, 0, *addrlen);
    return fd;
#else
    fd = ::accept(s, addr, static_cast<QT_SOCKLEN_T *>(addrlen));
#endif
    if (fd == -1)
        return -1;

    ::fcntl(fd, F_SETFD, FD_CLOEXEC);

    // set non-block too?
    if (flags & O_NONBLOCK)
        ::fcntl(fd, F_SETFL, ::fcntl(fd, F_GETFL) | O_NONBLOCK);

    return fd;
}

// UnixWare 7 redefines listen -> _listen
static inline int qt_safe_listen(int s, int backlog, sockaddr *from_addr, QT_SOCKLEN_T *from_addr_len)
{
    int error = ::listen(s, backlog);    

#ifdef _GUARDIAN_TARGET
    error = ::accept_nw(s, from_addr, from_addr_len, acceptnw_tag/*(long)from_addr*/ /*as_tag*/);
    Q_ASSERT(error == 0);
#endif
    return error;
}

static inline int qt_safe_connect(int sockfd, const struct sockaddr *addr, QT_SOCKLEN_T addrlen)
{
    register int ret;
    // Solaris e.g. expects a non-const 2nd parameter
#ifdef _GUARDIAN_TARGET
    int tag;
    ret = ::connect_nw((short)sockfd, const_cast<struct sockaddr *>(addr), addrlen, connect_tag);
    short tfd = sockfd;
    _cc_status     CC;
    bool done = false;
    while (!done) {
        CC = AWAITIOX(&tfd,,,&tag, -1);
        if (_status_lt(CC) || tag == connect_tag) {
            done = true;
        }
    }

    if (_status_lt(CC))
        return -1;
    else
        return 0;
#else
    EINTR_LOOP(ret, QT_SOCKET_CONNECT(sockfd, const_cast<struct sockaddr *>(addr), addrlen));
#endif
    return ret;
}
#undef QT_SOCKET_CONNECT
#define QT_SOCKET_CONNECT qt_safe_connect

#if defined(socket)
# undef socket
#endif
#if defined(accept)
# undef accept
#endif
#if defined(listen)
# undef listen
#endif

// VxWorks' headers specify 'int' instead of '...' for the 3rd ioctl() parameter.
template <typename T>
#ifdef _GUARDIAN_TARGET
static inline int qt_safe_ioctl(short sockfd, int request, T arg)
#else
static inline int qt_safe_ioctl(int sockfd, int request, T arg)
#endif
{
#ifdef _GUARDIAN_TARGET
    short err =::socket_ioctl_nw(sockfd, request, (char*)arg, ioctl_tag);
    QCoreApplication::processEvents();
    return err;
#endif

#ifdef Q_OS_VXWORKS
    return ::ioctl(sockfd, request, (int) arg);
#else
    return ::ioctl(sockfd, request, arg);
#endif
}

// VxWorks' headers do not specify any const modifiers
static inline in_addr_t qt_safe_inet_addr(const char *cp)
{

#ifdef Q_OS_VXWORKS
    return ::inet_addr((char *) cp);
#else
    return ::inet_addr(cp);
#endif
}

// VxWorks' headers do not specify any const modifiers
static inline int qt_safe_sendto(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *to, QT_SOCKLEN_T tolen)
{
#ifdef MSG_NOSIGNAL
    flags |= MSG_NOSIGNAL;
#else
#ifndef _GUARDIAN_TARGET
    qt_ignore_sigpipe();
#endif
#endif

    register int ret;
#if defined(_GUARDIAN_TARGET)
    EINTR_LOOP(ret, ::sendto(sockfd, (char *) buf, len, flags, (struct sockaddr *) to, tolen));
#else
#ifdef Q_OS_VXWORKS
    EINTR_LOOP(ret, ::sendto(sockfd, (char *) buf, len, flags, (struct sockaddr *) to, tolen));

#else
    EINTR_LOOP(ret, ::sendto(sockfd, buf, len, flags, to, tolen));
#endif
#endif
    return ret;
}

QT_END_NAMESPACE

#endif // QNET_GUARDIAN_P_H
