TARGET = qsqldb2

SOURCES = main.cpp
INCLUDEPATH+=/opt/ibm/dsdriver/include
LIBS+=-L/opt/ibm/dsdriver/lib -ldb2

include(../../../sql/drivers/db2/qsql_db2.pri)

include(../qsqldriverbase.pri)
