#include "greceivemanager.h"

#include "qdebug.h"

#include <tal.h>
#include <cextdecs.h>

//#define GRECEIVEMANAGER_DEBUG

GReceiveManager::GReceiveManager(QObject *parent) :
  QObject(parent)
{
#ifdef GRECEIVEMANAGER_DEBUG
    qDebug()<<"GReceiveManager::GReceiveManager()";
#endif
    init();
}

void 
GReceiveManager::readSomethingNoWait() 
{
#ifdef  GRECEIVEMANAGER_DEBUG
     qDebug()<<"GReceiveManager::readSomethingNoWait()";
#endif

     short error = READUPDATEX(fd_receive_, 
                               (char*)last_read_, 
                               MAX_BLOCK_SIZE, , 13 /*receive_read_tag*/);  
     if (error != 0) {
         FILE_GETINFO_(fd_receive_, &error);
         qDebug()<<"guardian error is:"<<error;
         qFatal("GReceiveManager::readSomethingNoWait programmatic error");
     }
}

void
GReceiveManager::processCreate()
{
#ifdef GRECEIVEMANAGER_DEBUG
    qDebug()<<"GReceiveManager::processCreate()";
#endif

    short nowait_tag;
    memcpy(&nowait_tag, (void*)(last_read_+1),2);

    short  *proc_handler = new short[10];

    PROCESSHANDLE_NULLIT_ ( proc_handler );

    memcpy(proc_handler, (void*)(last_read_+3),10*2);
    short error = last_read_[13];
    short error_detail = last_read_[14];
    short len_proc_descriptor = last_read_[15];
    
    int nowait64_tag = 0;
    memcpy(&nowait64_tag, (void*)(last_read_+16),4);
    char process_descriptor[1024];
    memset(process_descriptor,0, 1024);
    memcpy(&process_descriptor, (void*)(last_read_+20),len_proc_descriptor);
 
#ifdef GRECEIVEMANAGER_DEBUG
    qDebug()<<"tag:"<< nowait_tag;
    qDebug()<<"handler:"<< proc_handler[0]<<proc_handler[1]
            <<proc_handler[2]<<proc_handler[3]
            <<proc_handler[4]<<proc_handler[5]
            <<proc_handler[6]<<proc_handler[7]
            <<proc_handler[8]<<proc_handler[9];

    qDebug()<<"error:"<< error;
    qDebug()<<"error_detail:"<< error_detail;
    qDebug()<<"len_proc_descriptor:"<< len_proc_descriptor;
    qDebug()<<"nowait64_tag:"<< nowait64_tag;
    qDebug()<<"process descriptor"<<process_descriptor;
#endif

    if (error != 0) {
        emit onProcessCreateFailed();
        return;
    }

    emit onProcessCreateSucceeded(proc_handler);

    return;
}

void
GReceiveManager::processDelete()
{
#ifdef GRECEIVEMANAGER_DEBUG
    qDebug()<<"GReceiveManager::processDelete()";
#endif
    short  *proc_handler = new short[10];
    PROCESSHANDLE_NULLIT_ ( proc_handler );
    memcpy(proc_handler, (void*)(last_read_+1),10*2);
    int proc_time = 0;
    memcpy(&proc_time, (void*)(last_read_+11),4);
    short completion_code = last_read_[16];
#ifdef GRECEIVEMANAGER_DEBUG
    qDebug()<<"handler:"<< proc_handler[0]<<proc_handler[1]
            <<proc_handler[2]<<proc_handler[3]
            <<proc_handler[4]<<proc_handler[5]
            <<proc_handler[6]<<proc_handler[7]
            <<proc_handler[8]<<proc_handler[9];
    qDebug()<<"proc_time:"<< proc_time;
#endif

    emit onProcessDeleted(proc_handler, completion_code);
}

void
GReceiveManager::onReceiveWrite(int xfr)
{
#ifdef  GRECEIVEMANAGER_DEBUG
     qDebug()<<"GReceiveManager::onReceiveWrite()";
#endif
}

void
GReceiveManager::onReceiveRead(int xfr)
{
#ifdef  GRECEIVEMANAGER_DEBUG
     qDebug()<<"GReceiveManager::onReceiveRead()";
     qDebug()<<"xfr="<<xfr;
#endif
     
     switch(last_read_[0]) {
     case -101: processDelete();break;
     case -102: processCreate();break;
         //case -103: /*process open*/;break;
         //case -104: /*process close*/;break;
     default  : 
         qWarning(("Unhandeled system message!"+
                   QString::number(last_read_[0])).toLatin1().data());break;
         readSomethingNoWait();
         return;
     };

     short buff[17];
     short error = FILE_GETRECEIVEINFO_(buff);     
     if (error != 0) {
         qDebug()<<"guardian error is:"<<error;
         readSomethingNoWait();
         return;
     }

     error = REPLYX(,,,buff[2],0);

     if (error != 0) {
         qWarning(("Error in REPLYX : "+
                   QString::number(error)).toLatin1().data());
         readSomethingNoWait();
         return;
     }
     
     readSomethingNoWait();
}

void
GReceiveManager::init()
{
#ifdef GRECEIVEMANAGER_DEBUG
    qDebug()<<"GReceiveManager::init()";
#endif
    fd_receive_ = 0;

    unsigned nowait_mask = 16384;
    short error = FILE_OPEN_("$receive",
                  8,
                  &fd_receive_,
                  ,
                  ,
                  1 /*nowait depth*/,
                  2 /*receive depth*/,
                  nowait_mask);

    int nowait_tag = 0;
    AWAITIOX(&fd_receive_, ,  , &nowait_tag, -1);

    if (fd_receive_ < 0) {
        qFatal("GReceiveManager::init() programmatic error");
    }

    FILE_GETINFO_(fd_receive_, &error);
    if (error != 0) {
        FILE_GETINFO_(fd_receive_, &error);
        qDebug()<<"guardian error is:"<<error;
        qFatal("GReceiveManager::init() programmatic error");
    }
   
#ifdef GRECEIVEMANAGER_DEBUG
    qDebug()<<"GReceiveManager::init: $receive opened successfully";
#endif

    unsigned mask = 7;
    error = SETMODE(fd_receive_, 80, mask);
    if (error < 0) {
        FILE_GETINFO_(fd_receive_, &error);
        qDebug()<<"guardian error is:"<<error;
        qFatal("GReceiveManager::init() programmatic error");
    }

    readSomethingNoWait();           
}
