/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtCore module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QSOCKETNOTIFIER_H
#define QSOCKETNOTIFIER_H

#include <QtCore/qobject.h>

QT_BEGIN_HEADER

QT_BEGIN_NAMESPACE

QT_MODULE(Core)

class Q_CORE_EXPORT QSocketNotifier : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QObject)

public:
    enum Type { Read, Write, Exception };

#ifdef _GUARDIAN_TARGET
    QSocketNotifier(int socket, Type, double tag, QObject *parent = 0);
#else
    QSocketNotifier(int socket, Type, QObject *parent = 0);
#endif

#ifdef QT3_SUPPORT
    QT3_SUPPORT_CONSTRUCTOR QSocketNotifier(int socket, Type, QObject *parent, const char *name);
#endif
    ~QSocketNotifier();

    inline int socket() const { return sockfd; }
    inline Type type() const { return sntype; }

#ifdef _GUARDIAN_TARGET
    inline short error() const { return awerror_; }
    inline double tag() const { return stag_; }
    inline double transferred() const { return stransferred_; }

    inline void setError(short err) { awerror_ = err; }
    inline void setTag(double tag) { stag_ = tag; }
    inline void setTransferred(unsigned short num) { stransferred_ = num; }
    inline void setConnecting(bool flg) { connecting_ = flg; } 
    inline bool isConnecting() const { return connecting_; }
#endif
    inline bool isEnabled() const { return snenabled; }

public Q_SLOTS:
    void setEnabled(bool);

Q_SIGNALS:
    void activated(int socket);

protected:
    bool event(QEvent *);

private:
    Q_DISABLE_COPY(QSocketNotifier)

    int sockfd;
    Type sntype;
    bool snenabled;

#ifdef _GUARDIAN_TARGET
    double stag_;
    unsigned short stransferred_;
    bool  connecting_;
    short awerror_;
#endif

};

QT_END_NAMESPACE

QT_END_HEADER

#endif // QSOCKETNOTIFIER_H
