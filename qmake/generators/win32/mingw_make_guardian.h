#ifndef MINGW_MAKE_GUARDIAN_H
#define MINGW_MAKE_GUARDIAN_H

#include "winmakefile.h"

QT_BEGIN_NAMESPACE

class GuardianMakefileGenerator : public Win32MakefileGenerator
{
public:
    GuardianMakefileGenerator();
    ~GuardianMakefileGenerator();
protected:
    QString escapeDependencyPath(const QString &path) const;
    QString getLibTarget();
    bool writeMakefile(QTextStream &);
    void init();
private:
    bool isWindowsShell() const;
    void writeMingwParts(QTextStream &);
    void writeIncPart(QTextStream &t);
    void writeLibsPart(QTextStream &t);
    void writeLibDirPart(QTextStream &t);
    void writeObjectsPart(QTextStream &t);
    void writeBuildRulesPart(QTextStream &t);
    void writeRcFilePart(QTextStream &t);
    void processPrlVariable(const QString &var, const QStringList &l);
    QStringList &findDependencies(const QString &file);
    
    QString preCompHeaderOut;

    virtual bool findLibraries();
    bool findLibraries(const QString &where);
    void fixTargetExt();

    bool init_flag;
    QString objectsLinkLine;
    QString quote;
};

inline GuardianMakefileGenerator::~GuardianMakefileGenerator()
{ }

QT_END_NAMESPACE

#endif // MINGW_MAKE_H
